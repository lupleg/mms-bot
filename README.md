# MMS Bot

## Introduction
MMS Bot is a chatbot developed using Rasa, an open-source conversational AI framework. This bot is designed to handle various tasks related to MMS (Multimedia Messaging Service).

## Features
- **Intent Recognition**: The bot can understand user intents and respond accordingly.
- **Entity Extraction**: It can extract relevant information from user messages.
- **Natural Language Understanding**: The bot uses machine learning techniques to understand user queries.
- **Contextual Conversations**: It can maintain context and carry out multi-turn conversations.
- **Integration**: The bot can be integrated with various messaging platforms like Slack, Facebook Messenger, etc.

## Installation
1. Clone the repository: `git clone https://gitlab.com/lupleg/mms-bot.git`
2. Install the required dependencies: `pip install -r requirements.txt`
3. Train the Rasa model: `rasa train`
4. Start the Rasa server: `rasa run`
5. Connect the bot to a messaging platform of your choice using Rasa connectors.
6. Follow this link to read more about [RASA COMMANDS](https://rasa.com/docs/rasa/command-line-interface)

## Usage
1. Send a message to the bot using the connected messaging platform.
2. The bot will recognize the intent and respond accordingly.
3. You can have multi-turn conversations with the bot by maintaining context.

## Contributing
Contributions are welcome! If you find any issues or have suggestions for improvement, please open an issue or submit a pull request.

## License
This project is licensed under the [MIT License](LICENSE).

## Contact
For any questions or inquiries, please contact [dev@lupleg.website](mailto:dev@lupleg.website).
